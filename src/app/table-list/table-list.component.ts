import { Component, OnInit } from '@angular/core';
import { Air } from '../models/Air';
import { AirlinesService } from '../shared/airlines.service';
import { delay } from 'rxjs/operators';


@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss']
})
export class TableListComponent implements OnInit {

  displayedColumns: string[] = ['flightNumber', 'company', 'status', 'terminal', 'time'];
  dataSource: Air[] = [];
  isLoad: boolean = false;
  selected: string = '';
  flightNumber: number;
  isAllReset: boolean = false;

  constructor(private airlinesService: AirlinesService) { }

  ngOnInit() {
    this.airlinesService.fetch().pipe(delay(3000)).subscribe((data: Air[]) => {
      this.dataSource = data;

      this.isLoad = true;
    })
  }
}
