import { Pipe, PipeTransform } from '@angular/core';
import { Air } from '../../models/Air';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(airLines: Air[], fligthNumber: number, status: string = ''): any {
    const number = fligthNumber ? String(fligthNumber) : '';
     
    return airLines.filter(air => String(air.flightNumber).includes(number) && air.status.includes(status));
  }

}
