import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Air } from "../models/Air";
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class AirlinesService {
    constructor(private http: HttpClient) {}

    fetch(): Observable<Air[]> {
        return this.http.get<Air[]>('http://localhost:3000/airlines');
    }
}