import { NgModule } from "@angular/core";

import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';

const material = [
    MatInputModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatButtonModule
]

@NgModule({
    imports: material,
    exports: material
})
export class MaterialModule {}