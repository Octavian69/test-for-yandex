export interface Air {
    company: string;
    flightNumber: number;
    status: string;
    terminal: string;
    time: string;
}
